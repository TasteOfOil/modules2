function _plus(a,b){
    return a + b;
}

function _multiply(a,b){
    return a * b;
}

module.exports = {_plus, _multiply};